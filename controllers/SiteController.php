<?php

namespace app\controllers;

use app\extensions\Controller;
use app\models\AuthKey;
use app\models\User;
use general\ext\api\auth\AuthApi;
use general\ext\api\auth\AuthUrlCreator;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SiteController extends Controller {
	public $layout = 'emptyHtml';
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}