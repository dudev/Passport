<?php

namespace app\controllers\api;

use app\models\api\PassportSearch;
use app\models\Passport;
use general\controllers\api\Controller;
use yii\db;

class PassportController extends Controller {
	public function actionSearchById(array $ids = [], array $fields = []) {
		if(!$ids) {
			$this->sendError(self::ERROR_NO_PASSPORT);
		}

		$res = Passport::find();
		$fields_without_rel = $fields;
		if($fields) {
			foreach (['additional_information', 'additional_email', 'additional_phone'] as $val) {
				if(($key = array_search($val, $fields_without_rel)) !== false) {
					unset($fields_without_rel[ $key ]);
				}
			}
			$fields_without_rel = array_values($fields_without_rel);
			$res->select($fields);
		}
		$res->where(['id' => $ids]);

		if(!$res = $res->all()) {
			$this->sendError(self::ERROR_NO_PASSPORT);
		}

		$passports = [];
		foreach ($res as $val) {
			/* @var $val Passport */
			$passports[] = array_merge(
				$val->getAttributes($fields ? $fields_without_rel : null, ['created_at', 'updated_at']),
				(array_search('additional_information', $fields_without_rel) !== false) || !$fields
					? [
						'additional_information' => empty($val->passportAdditionalInformation)
							? []
							: $val->passportAdditionalInformation->info,
					]
					: [],
				(array_search('additional_email', $fields_without_rel) !== false) || !$fields
					? [
						'additional_email' => [],
					]
					: [],
				(array_search('additional_phone', $fields_without_rel) !== false) || !$fields
					? [
						'additional_phone' => [],
					]
					: []
			);
		}
		return $this->sendSuccess(['passports' => $passports]);
	}
    public function actionSearch($page = 0) {
	    if($page > 100) {
		    return $this->sendError(self::ERROR_TOO_BIG_PAGE_NUMBER);
	    }

	    $data = (new PassportSearch())->search(\Yii::$app->request->post(), $page);

	    if(is_int($data)) {
		    return $this->sendError($data);
	    }

	    if($data === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    return $this->sendSuccess($data);
    }
	public function actionCreate() {
		if(\Yii::$app->request->isGet) {
			return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
		}
		$model = new Passport();
		$model->loadDefaultValues();

		if(!$model->load(\Yii::$app->request->post())) {
			return $this->sendError(self::ERROR_NO_DATA);
		}

		if($model->validate()) {
			if(!$model->save(false)) {
				return $this->sendError(self::ERROR_DB);
			}

			return $this->sendSuccess([
				'passport' => array_merge(
					$model->getAttributes([
						'id',
						'first_name',
						'last_name',
						'father_name',
						'phone',
						'email',
						'sex',
						'birthday',
						'user_id',
					]),
					[
						'additional_information' => empty($model->passportAdditionalInformation)
							? []
							: $model->passportAdditionalInformation->info,
						'additional_email' => [],
						'additional_phone' => [],
					]
				)
			]);
		} else {
			$errors = $this->getErrorCodes(
				[
					'first_name' => self::ERROR_ILLEGAL_PASSPORT_FIRSTNAME,
					'last_name' => self::ERROR_ILLEGAL_PASSPORT_LASTNAME,
					'father_name' => self::ERROR_ILLEGAL_PASSPORT_FATHERNAME,
					'phone' => self::ERROR_ILLEGAL_PASSPORT_PHONE,
					'email' => self::ERROR_ILLEGAL_PASSPORT_EMAIL,
					'birthday' => self::ERROR_ILLEGAL_PASSPORT_BIRTHDAY,
					'sex' => self::ERROR_ILLEGAL_PASSPORT_SEX,
					'user_id' => self::ERROR_ILLEGAL_PASSPORT_USER_ID,
					'info' => self::ERROR_ILLEGAL_PASSPORT_INFO,
				],
				$model);
		}

		return $this->sendError($errors);
	}
	public function actionDelete($id) {
		if($passport = Passport::findOne(['id' => $id])) {
			/* @var $passport Passport */
			if($passport->delete() === false) {
				return $this->sendError(self::ERROR_DB);
			} else {
				return $this->sendSuccess([]);
			}
		} else {
			return $this->sendError(self::ERROR_NO_PASSPORT);
		}
	}
	public function actionUpdate($id) {
		/* @var Passport $model */
		$model = Passport::find()
			->where(['id' => $id])
			->with('passportAdditionalInformation')
		//	->with('passportAdditionalPhone')
		//	->with('passportAdditionalEmail')
			->one();
		if(!$model) {
			return $this->sendError(self::ERROR_NO_POST);
		}
		if(\Yii::$app->request->isPost) {
			if (!$model->load(\Yii::$app->request->post())) {
				return $this->sendError(self::ERROR_NO_DATA);
			}

			if ($model->validate()) {
				if (!$model->save(false)) {
					return $this->sendError(self::ERROR_DB);
				}
			} else {
				$errors = $this->getErrorCodes(
					[
						'first_name' => self::ERROR_ILLEGAL_PASSPORT_FIRSTNAME,
						'last_name' => self::ERROR_ILLEGAL_PASSPORT_LASTNAME,
						'father_name' => self::ERROR_ILLEGAL_PASSPORT_FATHERNAME,
						'phone' => self::ERROR_ILLEGAL_PASSPORT_PHONE,
						'email' => self::ERROR_ILLEGAL_PASSPORT_EMAIL,
						'birthday' => self::ERROR_ILLEGAL_PASSPORT_BIRTHDAY,
						'sex' => self::ERROR_ILLEGAL_PASSPORT_SEX,
						'user_id' => self::ERROR_ILLEGAL_PASSPORT_USER_ID,
						'info' => self::ERROR_ILLEGAL_PASSPORT_INFO,
					],
					$model);
				return $this->sendError($errors);
			}
		}

		return $this->sendSuccess([
			'passport' => array_merge(
				$model->getAttributes(null, ['created_at', 'updated_at']),
				[
					'additional_information' => empty($model->passportAdditionalInformation)
						? []
						: $model->passportAdditionalInformation->info,
					'additional_email' => [],
					'additional_phone' => [],
				]
			)
		]);
	}
}
