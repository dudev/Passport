<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passport_additional_information".
 *
 * @property integer $passport_id
 * @property string $info
 *
 * @property Passport $passport
 */
class PassportAdditionalInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'passport_additional_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passport_id', 'info'], 'required'],
            [['passport_id'], 'integer'],
            [['info'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'passport_id' => 'Passport ID',
            'info' => 'Info',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassport()
    {
        return $this->hasOne(Passport::className(), ['id' => 'passport_id']);
    }
}
