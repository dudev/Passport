<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passport_additional_phone".
 *
 * @property integer $passport_id
 * @property string $phone
 *
 * @property Passport $passport
 */
class PassportAdditionalPhone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'passport_additional_phone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passport_id', 'phone'], 'required'],
            [['passport_id'], 'integer'],
            [['phone'], 'string', 'max' => 20],
            [['passport_id', 'phone'], 'unique', 'targetAttribute' => ['passport_id', 'phone'], 'message' => 'The combination of Passport ID and Phone has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'passport_id' => 'Passport ID',
            'phone' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassport()
    {
        return $this->hasOne(Passport::className(), ['id' => 'passport_id']);
    }
}
