<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passport_additional_email".
 *
 * @property integer $passport_id
 * @property string $email
 *
 * @property Passport $passport
 */
class PassportAdditionalEmail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'passport_additional_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passport_id', 'email'], 'required'],
            [['passport_id'], 'integer'],
            [['email'], 'string', 'max' => 50],
            [['passport_id', 'email'], 'unique', 'targetAttribute' => ['passport_id', 'email'], 'message' => 'The combination of Passport ID and Email has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'passport_id' => 'Passport ID',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassport()
    {
        return $this->hasOne(Passport::className(), ['id' => 'passport_id']);
    }
}
