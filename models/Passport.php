<?php

namespace app\models;

use general\ext\api\auth\AuthApi;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "passport".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $father_name
 * @property string $phone
 * @property string $email
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PassportAdditionalEmail[] $passportAdditionalEmails
 * @property PassportAdditionalPhone[] $passportAdditionalPhones
 */
class Passport extends ActiveRecord implements IdentityInterface
{
    public $info = '[]';

	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'passport';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['user_id', 'birthday'], 'integer'],
            [['first_name', 'last_name', 'father_name', 'phone'], 'string', 'max' => 20],
			[['email'], 'string', 'max' => 50],
            ['sex', 'default', 'value' => 0],
            ['sex', 'boolean'],
            ['phone', 'unique'],
            ['email', 'unique'],
            [['email', 'phone'], 'filter', 'filter' => function($value) {
                return empty($value) ? null : $value;
            }],
            ['user_id', 'unique'],
	        ['user_id', function ($attribute, $params) {
		        $res = AuthApi::userSearch(['id' => $this->user_id, 'status' => 1]);
		        if($res['result'] == 'error') {
			        $this->addError('user_id', 'Пользователь не найден, заблокирован, либо отсутствует связь с сервисом аутентификации.');
		        }
	        }],
			['info', 'string'],
        ];
    }

    public function save($runValidation = true, $attributeNames = null) {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if(parent::save($runValidation, $attributeNames)) {
                if ($this->getIsNewRecord() || is_null($this->passportAdditionalInformation)) {
                    $info = new PassportAdditionalInformation();
                    $info->passport_id = $this->id;
                } else {
                    $info = $this->passportAdditionalInformation;
                }
                $info->info = $this->info;
                if(!$info->save()) {
                    throw new Exception('Ошибка сохранения');
                }
            } else {
                throw new Exception('Ошибка сохранения');
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'father_name' => 'Father Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassportAdditionalEmails()
    {
        return $this->hasMany(PassportAdditionalEmail::className(), ['passport_id' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPassportAdditionalInformation()
	{
		return $this->hasOne(PassportAdditionalInformation::className(), ['passport_id' => 'id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassportAdditionalPhones()
    {
        return $this->hasMany(PassportAdditionalPhone::className(), ['passport_id' => 'id']);
    }

	/**
	 * Finds an identity by the given ID.
	 * @param string|integer $id the ID to be looked for
	 * @return IdentityInterface the identity object that matches the given ID.
	 * Null should be returned if such an identity cannot be found
	 * or the identity is not in an active state (disabled, deleted, etc.)
	 */
	public static function findIdentity($id)
	{
		return Passport::findOne($id);
	}

	/**
	 * Finds an identity by the given token.
	 * @param mixed $token the token to be looked for
	 * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
	 * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
	 * @throws NotSupportedException
	 * @return IdentityInterface the identity object that matches the given token.
	 * Null should be returned if such an identity cannot be found
	 * or the identity is not in an active state (disabled, deleted, etc.)
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Returns an ID that can uniquely identify a user identity.
	 * @return string|integer an ID that uniquely identifies a user identity.
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Returns a key that can be used to check the validity of a given identity ID.
	 *
	 * The key should be unique for each individual user, and should be persistent
	 * so that it can be used to check the validity of the user identity.
	 *
	 * The space of such keys should be big enough to defeat potential identity attacks.
	 *
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 * @throws NotSupportedException
	 * @return string a key that is used to check the validity of a given identity ID.
	 * @see validateAuthKey()
	 */
	public function getAuthKey()
	{
		throw new NotSupportedException('"getAuthKey" is not implemented.');
	}

	/**
	 * Validates the given auth key.
	 *
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 * @param string $authKey the given auth key
	 * @throws NotSupportedException
	 * @return boolean whether the given auth key is valid.
	 * @see getAuthKey()
	 */
	public function validateAuthKey($authKey)
	{
		throw new NotSupportedException('"validateAuthKey" is not implemented.');
	}
}
