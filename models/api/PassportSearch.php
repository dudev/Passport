<?php
/**
 * Project: passport
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\models\api;


use app\models\Passport;
use general\controllers\api\Controller;
use yii\data\Pagination;

class PassportSearch extends Passport {
	public function rules()
	{
		return [
			['user_id', 'integer'],
			[['first_name', 'last_name', 'father_name', 'phone', 'email'], 'string'],
			['birthday', 'filter', 'filter' => function($value) {
				if(!$value = strtotime($value)) {
					$value = null;
				}
				return $value;
			}]
		];
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @param int $page
	 * @return array
	 */
	public function search($params, $page = 0) {
		$query = Passport::find()
			->with('passportAdditionalInformation')
		//	->with('passport-additional-phone')
		//	->with('passport-additional-email')
		;

		// load the search form data and validate
		if ($this->load($params, 'search') && $this->validate()) {
			$query->andFilterWhere([
				'user_id' => $this->user_id,
				'birthday' => $this->birthday,
				'email' => $this->email,
			]);

			$query->andFilterWhere(['like', 'first_name', $this->first_name])
				->andFilterWhere(['like', 'last_name', $this->last_name])
				->andFilterWhere(['like', 'father_name', $this->father_name])
				->andFilterWhere(['like', 'phone', $this->phone]);
		}

		$count_query = clone $query;
		$total_count = $count_query->limit(-1)->offset(-1)->orderBy([])->count();
		if($total_count == 0) {
			return Controller::ERROR_NO_PRODUCT;
		}
		$pages = new Pagination(['totalCount' => $total_count]);
		$pages->setPage($page - 1);
		$pages->setPageSize(30);

		$query->offset($pages->offset)
			->limit($pages->limit);

		if ($sort = \Yii::$app->request->get('sort')) {
			if(strncmp($sort, '-', 1) === 0) {
				$query->orderBy([substr($sort, 1) => SORT_DESC]);
			} else {
				$query->orderBy([$sort => SORT_ASC]);
			}
		}

		$passports = [];
		foreach ($query->all() as $val) {
			/* @var $val Passport */
			$passports[] = array_merge(
				$val->getAttributes([
					'id',
					'first_name',
					'last_name',
					'father_name',
					'phone',
					'email',
					'sex',
					'birthday',
					'user_id',
				]),
				[
					'additional_information' => empty($val->passportAdditionalInformation)
						? []
						: $val->passportAdditionalInformation->info,
					'additional_email' => [],
					'additional_phone' => [],
				]
			);
		}

		return [
			'passports' => $passports,
			'totalCount' => $total_count,
			'page' => $pages->getPage()
		];
	}
} 