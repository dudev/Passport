<?php

use yii\db\Schema;
use yii\db\Migration;

class m150329_041945_create_passport_additional_information_table extends Migration
{
    public function up()
    {
	    $this->createTable('passport_additional_information', [
		    'passport_id' => Schema::TYPE_INTEGER . ' PRIMARY KEY',
		    'info' => Schema::TYPE_TEXT . ' NOT NULL',
	    ]);
	    $this->addForeignKey(
		    'passport_id_FK_passport_additional_information',
		    'passport_additional_information',
		    'passport_id',
		    'passport',
		    'id',
		    'CASCADE',
		    'CASCADE'
	    );
    }

    public function down()
    {
        echo "m150329_041945_create_passport_additional_information_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
