<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin([
    'id' => 'loginForm',
    'fieldConfig' => [
        'template' => '<div class="field">{input}</div>',
    ],
]); ?>

<div id="logo">
    <img src="/i/logo.jpg" alt="Семь огней - Seven lights" title="Семь огней - Seven lights">
</div>

<?= $form->errorSummary($model, ['header' => '']) ?>

<?= $form->field(
    $model,
    'login',
    [
        'inputOptions' => [
            'placeholder' => $model->getAttributeLabel('login'),
        ]
    ]
) ?>
<?= $form->field($model, 'password',
    [
        'inputOptions' => [
            'placeholder' => $model->getAttributeLabel('password'),
        ]
    ]
)->passwordInput() ?>
<?= $form->field($model, 'invite',
    [
        'inputOptions' => [
            'placeholder' => $model->getAttributeLabel('invite'),
        ]
    ]
) ?>

<div class="submit">
    <a href="#" id="forgot">Забыли пароль?</a><?= Html::submitButton('Регистрация', ['name' => 'signup-button']) ?>
</div>

<?php ActiveForm::end(); ?>